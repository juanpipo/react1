import 'react-datepicker/dist/react-datepicker.min.css'
import React from 'react'
import './Form.css'
import sweetalert  from 'sweetalert'
import DatePicker from 'react-datepicker'
import moment from 'moment'


export default class Form extends React.Component {

    state= {

        fecha:new Date(),
        peso:""
    }

    onSubmit = ()=>{
        // Se captura lo que contenga el input de fecha y de peso
        const {fecha, peso} = this.state

        //console.log(fecha,peso)
        if(!peso || peso < 0){
            sweetalert ('Lectura invalida', 'El registro de peso debe ser valido', 'error')
        } else {
            this.props.onAceptar(this.state)
        }
    }

    cambioFecha =(fecha)=>{
        //console.log(evt)
        this.setState({fecha})
    }

    cambiarPeso =(evt)=>{
        this.setState({
            peso:evt.target.value
        })
    }

    render() {
        return(

            <div className="row">
                <div className={`form-container scale-transition scale-out ${this.props.visible ? "scale-in":"" } col s4 offset-s4 z-depth-4 cyan lighten-3`}>
                    <form>
                        <label htmlFor="fecha">
                            Fecha:<br></br>
                           <DatePicker
                            selected={this.state.fecha}
                            onChange={this.cambioFecha}
                            
                           />
                        </label> 
                        <br/>
                        <label htmlFor="peso">
                            Peso:
                            <input type="text" value={this.state.peso} name="peso" onChange={this.cambiarPeso} id="peso" />
                        </label>
                        <input type="button" className="btn" onClick={this.onSubmit} value="Agregar" />
                        <input type="button" className="btn" onClick={()=>this.props.onCerrar()} value="Cerrar" />
                    </form>
                </div>
            </div>
        );

    }
    
}