import React from "react";
import moment from "moment";

/* Componente de representacion */
export default ({registros}) => {

  /* Se encarga de renderizar cada una de las filas y las filas de la tabla*/
  const renderFila = registro=>{
    return (
      <tr key={registro[0]}>
        <td>{moment(registro[0]).format("LLLL")}</td>
        <td>{registro[1]}</td>
      </tr>
    );
  };

  return (
    <table className="z-depth-2 hoverable">
      <thead>
        <tr>
          <th>Fecha</th>
          <th>Peso (lb)</th>
        </tr>
      </thead>
      <tbody>
        {registros.map(registro => renderFila(registro))}
      </tbody>
    </table>
  );
};
