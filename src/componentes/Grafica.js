import React from 'react'
import Highcharts from "highcharts";




class Grafica extends React.Component {

    // ejecutando elmetodo iniciar grafica y le pasamos la propiedad registros
    componentDidMount(){
        this.iniciarGrafica(this.props.registros)
    }

    // Metodo especial del ciclo de vida del componente
    // Se encarga de actualizar el componente una ves que este montado
    // Decide si las nuevas propiedades son diferentes a la anteriores si son diferentes renderiza el componente
    componentWillReceiveProps(nextProps) {
      this.iniciarGrafica(nextProps.registros)
    }

    iniciarGrafica = (registros)=>{
        Highcharts.chart("grafica", {
            title: {
              text: "Mi registro de peso"
            },
            xAxis: {
              type: "datetime"
            },
            series: [
              {
                name: "test",
                data: registros
              }
            ]
          });
    }

    render() {
        return (
            <div id="grafica" />
        );
    }
}

export default Grafica