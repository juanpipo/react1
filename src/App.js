import React, { Component } from "react";
import BarraTitulo from "./componentes/BarraTitulo";
import Highcharts from "highcharts";
import moment from "moment";
import Grafica from "./componentes/Grafica"
import Tabla from "./componentes/Tabla"
import Form from "./componentes/Form"

/* Tanto el componente tabla como el grafica son importados dentro del componente padre app
   quien se encarga de mantener el estado que son los registros y pasarselos a cada uno de los componentes por separado */


moment.locale("es");

class App extends Component {

  state = {
    registros: [],
    modal:false
  }

  componentDidMount() {
    
  }

  aceptarRegistro =({peso, fecha})=>{
      // signo de mas dentro del array transformamos en el formato que necesitamos para la grafica
      // el signo + delante del peso quiere decir que lo estamos convirtiendo en un valor numerico
      const nuevoregistro = [+fecha, +peso]
      // estamos pasando una funcion que utiliza dos argumentos
      // esta funcion va a tener acceso a esos dos objetos fecha y peso
      // prevState hace referencia al estado previo del componente
      // props hace referencia a las propiedades
      console.log(nuevoregistro)
      this.setState((prevState,props)=>({
          registros:[...prevState.registros,nuevoregistro]
      }))
  }

  onCerrarForm = ()=>{
    this.setState({
      modal:false
    })
  }

  render() {
    const btnAdd= {
      position:"absolute",
      top:"80%",
      right:"10%"
    }

    // propiedad visible dentro del form es la que va a controlar si o no esta visible en pantalla
    return (
      <div>
        <Form
           visible={this.state.modal} onAceptar={this.aceptarRegistro} onCerrar={this.onCerrarForm} />
        <BarraTitulo />
        <main>
          <div className="valign-wrapper">
            <h3>Registro diario de peso</h3>
          </div>
          <div className="row">
            <div className="col l6 m12 s12">
              <Grafica registros={this.state.registros}/>
            </div>
            <div className="col l6 m12 s12">
              <Tabla registros={this.state.registros} />
            </div>
          </div>
          <a className="btn-floating btn-large waves-effect waves-light red" 
             style={btnAdd} onClick={()=> this.setState({modal:true})}>
             <i className="material-icons">add</i>
          </a>
        </main>
        Bienvenidos a mi aplicacion
      </div>
    );
  }
}

export default App;
